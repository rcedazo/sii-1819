#include <stdio.h>
#include <sys/time.h> 
#include <sys/types.h>    
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

int main(void)
{
  struct sockaddr_in server_addr,client_addr;
  int s, res, clilen;
  int num[2]; 

  s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s < 0) {
    printf("Error en la llamada socket\n");
    return 1;
  }

  //bzero((char *)&server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_port = htons(4200);


  int on=1;
  setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

  if (bind(s, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
    printf("Error: llamada bind\n");
    return 1;
  }

  clilen = sizeof(client_addr);
  
  while (1){
    /* recibe la petición, dos números enteros */
    recvfrom(s, (char *) num, 2 *sizeof(int), 0, (struct sockaddr *)&client_addr, &clilen);

    /* los datos se transforman del formato de red al del computador */
    res = ntohl(num[0]) + ntohl(num[1]); /* obtiene el resultado */

    printf ("Sumando %d + %d\n", ntohl(num[0]), ntohl(num[1]));
    res = htonl(res);

    /* el resultado se transforma a formato de red */
    /* envía el resultado y cierra la conexión */
    sendto(s, (char *)&res, sizeof(int), 0, (struct sockaddr *)&client_addr, clilen);
      
  }
  close (s);
  return 0;
}


