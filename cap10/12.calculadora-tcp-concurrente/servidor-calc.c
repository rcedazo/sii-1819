#include <stdio.h>
#include <sys/time.h> 
#include <sys/types.h>    
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void * suma(void *arg) {
     int num1, num2, res; 
     char mensaje1[50]="Escribe el numero 1";
     char mensaje2[50]="Escribe el numero 2";
     int sc;

     sc = (int)arg; 

    // Envia el primer mensaje
    if (write(sc, mensaje1, strlen(mensaje1)+1) < 0){
      printf("Error en write\n");
      return NULL;
    }

    /* recibe el primer número entero */
    if (read(sc, (char *)&num1, sizeof(int)) < 0){
      printf("Error en read\n");
      return NULL;
    }
    num1 = ntohl(num1); 
    printf("Recibido num1: %d\n", num1);

    // Envia el segundo mensaje
    if (write(sc, mensaje2, strlen(mensaje2)+1) < 0){
      printf("Error en write\n");
      return NULL;
    }

    /* recibe el primer número entero */
    if (read(sc, (char *)&num2, sizeof(int)) < 0){
      printf("Error en read\n");
      return NULL;
    }
    num2 = ntohl(num2); 
    printf("Recibido num2: %d\n", num2);

    res = num1 + num2; /* obtiene el resultado */
    res = htonl(res);
    /* el resultado se transforma a formato de red */
    /* envía el resultado y cierra la conexión */
    if (write(sc, (char *)&res, sizeof(int)) < 0){
      printf("Error en write\n");
      return NULL;
    }
    close(sc);
  
    pthread_exit(0);

}

int main(void)
{
  struct sockaddr_in server_addr,client_addr;
  int sd, sc;
  int size;

  pthread_t threads[100];
  int contador = 0;

  sd = socket(AF_INET, SOCK_STREAM, 0);
  if (sd < 0) {
    printf("Error en la llamada socket\n");
    return 1;
  }

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_addr.sin_port = htons(4200);

  int on=1;
  setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

  if (bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
    printf("Error en la llamada bind\n");
    return 1;
  }
  listen(sd, 5);
  size = sizeof(client_addr);
  while (1){
    printf("esperando conexion\n");
    sc = accept(sd, (struct sockaddr *)&client_addr, &size);
    if (sc < 0){
      printf("Error en accpet\n");
      break;
    }
    // Imprime la IP del cliente
    printf("IP del cliente: %s\n", inet_ntoa(client_addr.sin_addr));

    pthread_create(&threads[contador], NULL, suma, (void*)sc);
    contador++;

  }
  close (sd);
  return 0;
}


