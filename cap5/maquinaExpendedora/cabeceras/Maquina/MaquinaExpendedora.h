// MaquinaExpendedora.h: interface for the MaquinaExpendedora class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAQUINAEXPENDEDORA_H__4B997F00_9760_4163_81C7_CEA81787D3F7__INCLUDED_)
#define AFX_MAQUINAEXPENDEDORA_H__4B997F00_9760_4163_81C7_CEA81787D3F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../Cajero/HWCajero.h"
#include "../Dominio/Ingresos.h"
#include "../Dominio/Devoluciones.h" 


class MaquinaExpendedora  
{
public:
	MaquinaExpendedora();
	virtual ~MaquinaExpendedora();
	void solicitarProducto();

};

#endif // !defined(AFX_MAQUINAEXPENDEDORA_H__4B997F00_9760_4163_81C7_CEA81787D3F7__INCLUDED_)
