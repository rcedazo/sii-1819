#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>

#define MAX_IT 6

struct punto
{
    int x;
    int y;
};

struct punto *pp;
struct punto *ph;

int main(void)
{
    int fd;
    struct stat bstat;
    int i;
    void *p;
    int tuberia[2], tuberia2[2];
    char testigo;
    pipe(tuberia);
    pipe(tuberia2);
    write(tuberia[1], &testigo, 1);
    fd = open ("/tmp/puntos", O_RDWR); //Fichero existe
    fstat(fd, &bstat);
    p= mmap(NULL,bstat.st_size,PROT_READ |
            PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);

    if (fork() != 0)
    {
        pp = p;
        close(tuberia[1]);
        close(tuberia2[0]);
        for ( i=0; i<=MAX_IT; i++)
        {
            read(tuberia[0], &testigo, 1);
            pp->x = i;
            pp->y = i*2;
            pp++;
            pp->x = i*3;
            pp->y = i*5;
            pp++; // Avanza hasta la siguiente estructura
            write(tuberia2[1], &testigo, 1);
        }
        wait(NULL);
        close(tuberia[0]);
        close(tuberia2[1]);
    }
    else
    {
        ph = p;
        close(tuberia2[1]);
        close(tuberia[0]);
        for (i=0; i<=MAX_IT; i++)
        {
            read(tuberia2[0], &testigo, 1);
            printf("x:%d, y:%d\n",ph->x, ph->y);
            ph++; // Avanza hasta la siguiente estructura
            write(tuberia[1], &testigo, 1);
        }
        close(tuberia2[0]);
        close(tuberia[1]);
    }
}
