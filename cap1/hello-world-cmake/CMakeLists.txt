# Indicamos el nombre que queremos que tenga nuestro proyecto
PROJECT(HelloWorld)

SET(HelloWorld_SRC helloWorld.c)

# Indicamos el nombre del ejecutable a partir del fuente
ADD_EXECUTABLE(HelloWorld ${HelloWorld_SRC})
